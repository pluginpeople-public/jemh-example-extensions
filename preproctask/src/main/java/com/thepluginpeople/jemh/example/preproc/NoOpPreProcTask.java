package com.thepluginpeople.jemh.example.preproc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.javahollic.jira.emh.api.IJEMHRuntimeConfig;
import com.javahollic.jira.emh.api.KeyValuePair;
import com.javahollic.jira.emh.api.config.ConfigField;
import com.javahollic.jira.emh.api.config.ConfigField.FieldType;
import com.javahollic.jira.emh.api.extension.preproctask.IJEMHMailHeaderPreProcTask;
import com.javahollic.jira.emh.api.extension.preproctask.PreProcTaskFailureException;

public class NoOpPreProcTask implements IJEMHMailHeaderPreProcTask
{
	private static final Logger LOG=Logger.getLogger(NoOpPreProcTask.class);
	
	public NoOpPreProcTask()
	{
	}

	@Override
	public String getVendor()
	{
		return "Example";
	}

	@Override
	public String getVendorUrl()
	{
		return "http://www.example.com";
	}
	
	@Override
	public Set<String> getHeadersHandled()
	{
		Set<String> headers=new HashSet<String>();
		headers.add("To");
		headers.add("Cc");
		return headers;
	}

	@Override
	public String filterHeader(String header, String value, IJEMHRuntimeConfig config, List<ConfigField> taskConfig)
			throws PreProcTaskFailureException
	{
		LOG.info("I ran");
		return value;
	}

	@Override
	public List<ConfigField> getDefaultConfig()
	{
		List<ConfigField> config=new ArrayList<ConfigField>();
		
		ConfigField autoConvertGroupToAddress=new ConfigField("groupHandling","remove","Control how an invalid addressee GroupName like [Your Accounts Dept] is handled",null, null);
		autoConvertGroupToAddress.getAttributes().setFieldType(FieldType.selectSingle);		
		autoConvertGroupToAddress.getAttributes().setWeight(10);
		List<KeyValuePair> options=new ArrayList<KeyValuePair>();
		options.add(new KeyValuePair("do this","do that"));
		options.add(new KeyValuePair("do that","do that"));
		autoConvertGroupToAddress.getAttributes().setOptions(options);
		config.add(autoConvertGroupToAddress);
		
		ConfigField emailAddressTemplate=new ConfigField("somefile", "%G@yourco.net", "Define the template on which a converted group will be based where %G is the replacement, eg: [Your Accounts Dept] after conversion becomes: your_accounts_dept@yourco.net", null,null);
		emailAddressTemplate.getAttributes().setWeight(20);
		config.add(emailAddressTemplate);

		ConfigField test1=new ConfigField("test1", "test1", "test1", null,null);
		test1.getAttributes().setWeight(1);
		test1.getAttributes().setFieldType(FieldType.checkbox);
		config.add(test1);

		ConfigField test100=new ConfigField("test100", "test100", "test100", null,null);
		test100.getAttributes().setFieldType(FieldType.selectMulti);	
		test100.getAttributes().setWeight(100);
		List<KeyValuePair> options2=new ArrayList<KeyValuePair>();
		options2.add(new KeyValuePair("x","x"));
		options2.add(new KeyValuePair("y","y"));
		options2.add(new KeyValuePair("z","z"));
		test100.getAttributes().setOptions(options2);
		config.add(test100);
		
		sortConfigFields(config);
		return config;
	}

	@Override
	public String getKey()
	{
		return "noop-preproctask";
	}

	@Override
	public String getName()
	{
		return "No-Op PreProcTask";
	}

	@Override
	public String getDescription()
	{
		return "An empty implementation for testing";
	}
	
	protected void sortConfigFields(List<ConfigField> fields)
	{
		Collections.sort(fields, new Comparator<ConfigField>(){
			@Override
			public int compare(ConfigField o1, ConfigField o2)
			{
				if (o1==null && o2!=null)
				{
					return -1;
				} 
				else if (o1!=null && o2==null)
				{
					return 1;
				}
				else if (o1==null && o2==null)
				{
					return 0;
				}
				else
				{
					return Integer.valueOf(o1.getAttributes().getWeight()).compareTo(Integer.valueOf(o2.getAttributes().getWeight()));
				}
			}});	
	}

	@Override
	public String getStatus()
	{
		return "ok";
	}

	@Override
	public boolean isUsable()
	{
		return true;
	}

	@Override
	public String filterPostProcessedMail(String arg0)
	{
		return arg0;
	}

	@Override
	public void init(IJEMHRuntimeConfig arg0, List<ConfigField> arg1)
	{
	}

	@Override
	public boolean isFilterPostProccessedMail()
	{
		return false;
	}

	@Override
	public boolean isSelfTestCapable()
	{
		return false;
	}

	@Override
	public boolean runSelfTest(List<ConfigField> arg0) throws PreProcTaskFailureException
	{
		return false;
	}
	
}
