package com.javahollic.jemh.example;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

import org.apache.log4j.Logger;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.service.util.handler.MessageHandlerErrorCollector;
import com.javahollic.jira.emh.api.IJEMHDirectiveHintSink;
import com.javahollic.jira.emh.api.IJEMHInboundReport;
import com.javahollic.jira.emh.api.IJEMHInboundReport.Sections;
import com.javahollic.jira.emh.api.IJEMHRuntimeConfig;
import com.javahollic.jira.emh.api.extension.InitFailureException;
import com.javahollic.jira.emh.api.extension.msgfilter.FilterFailureException;
import com.javahollic.jira.emh.api.extension.msgfilter.IJEMHMessageFilterProvider;

public class ExampleMessageFilter implements IJEMHMessageFilterProvider
{
	private static final Logger LOG=Logger.getLogger(ExampleMessageFilter.class);
	
	private String fReason;

	@Override
	public String getKey()
	{
		return "yourco-example-filter";
	}

	@Override
	public String getVendor()
	{
		return "Your co";
	}

	@Override
	public String getVendorUrl()
	{
		return "http://www.yourco.net";
	}

	@Override
	public String getName()
	{
		return "Example 3rd party JEMH filter";
	}

	@Override
	public String getDescription()
	{
		return "Provide an example skeleton of a filter";
	}

	@Override
	public String getReason()
	{
		return fReason;
	}

	@Override
	public void init() throws InitFailureException
	{
	}

	@Override
	public ProcessOutcome filter(Message message, IJEMHRuntimeConfig arg1, IJEMHDirectiveHintSink dirSink, MessageHandlerErrorCollector errorCollector, Issue issue, IJEMHInboundReport report) throws FilterFailureException
	{
		ProcessOutcome outcome=ProcessOutcome.canHandle;
		
		try
		{
			LOG.info("Example filter fired: "+message.getSubject());

			Address[] from = message.getFrom();
			if (from!=null && from.length>0)
			{
				InternetAddress inetAddress=(InternetAddress)from[0];
				String address=inetAddress.getAddress();
				if (address.endsWith("spammy.com"))
				{
					LOG.info("Mail determined to be spammpy due to domain : "+ address);
					outcome=ProcessOutcome.drop;
					report.addMessage(Sections.Filter, "SPAM message detected");
				}
			}
		}
		catch (MessagingException e)
		{
			e.printStackTrace();
			errorCollector.error("Failed to run filter", e);
		}
		return outcome;
	}

	@Override
	public String getStatus()
	{
		return "ok";
	}

	@Override
	public boolean isUsable()
	{
		return true;
	}

	@Override
	public boolean isLateRunning() {
		// TODO Auto-generated method stub
		return false;
	}


}
