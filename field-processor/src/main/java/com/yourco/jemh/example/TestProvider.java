package com.yourco.jemh.example;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

import com.atlassian.jira.issue.Issue;
import com.atlassian.mail.MailUtils;
import com.javahollic.jira.emh.api.IJEMHRuntimeConfig;
import com.javahollic.jira.emh.api.extension.fieldproc.FieldProcessorException;
import com.javahollic.jira.emh.api.extension.fieldproc.IJEMHFieldProcessor;
import com.javahollic.jira.emh.api.extension.fieldproc.IJEMHFieldProcessorContext;

/* Example Field Processor that owns mail from 'jemhtestfp@localhost.localdomain'
 * 
 * Kudos to Josh Vura-Weis (wtpooh@leland.Stanford.EDU) for http://mysite.verizon.net/ebrowne72/chef/JavaChef.zip
 * 
 */
public class TestProvider implements IJEMHFieldProcessor
{
	private Map<String, Object> fFieldMap = new HashMap<String, Object>();
	private Message fMessage;
	private IJEMHRuntimeConfig fConfig;
	private Issue fRelatedIssue;
	private String fBody;
	private IJEMHFieldProcessorContext fContext;
	
	public TestProvider()
	{
	}
	
	public boolean isHandlerOwnsMessage() {
		boolean owns = false;
		try {
			String senderAddress=((InternetAddress)fMessage.getFrom()[0]).getAddress();
			owns = senderAddress.equals("jemhtestfp@localhost.localdomain");
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		return owns;
	}

	
	public int getFieldCount() {
		
		int size=0;
		try
		{
			Collection<Map<String, Object>> maps = getFieldMaps();
			if (!maps.isEmpty())
			{
				size=maps.iterator().next().keySet().size();
			}
		}
		catch (FieldProcessorException fpe)
		{
			fpe.printStackTrace();
		}
		return size;
	}

	public Collection<Map<String, Object>> getFieldMaps() throws FieldProcessorException
	{
		if (isHandlerOwnsMessage())
		{
			try
			{
				String rawBody=MailUtils.getBody(fMessage);
				Encheferizer en=new Encheferizer();
				String enscheferizedBody=en.encheferize(rawBody);
				fFieldMap.put(KEY_SUMMARY, fMessage.getSubject());
				fFieldMap.put(KEY_COMMENT, enscheferizedBody);
				fFieldMap.put(KEY_PRIORITY, "critical");
				fFieldMap.put(KEY_PROJECT, "TEST");
				fFieldMap.put(KEY_REPORTER, "admin");
				fFieldMap.put(KEY_ISSUETYPE, "task");
				fFieldMap.put(KEY_SUMMARY, fMessage.getSubject()!=null?fMessage.getSubject():"Not provided :(");
			}
			catch (MessagingException e)
			{
				e.printStackTrace();
			} 
			catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		Collection<Map<String, Object>> maps=new ArrayList<Map<String, Object>>();
		maps.add(fFieldMap);
		return maps;
	}

	public String getKey() {
		return "example-fieldproc-mystery";
	}

	public String getName() {
		return "Mystery JEMH Field Processor";
	}

	public String getDescription() {
		return "Mystery FieldProcessor that owns mails sent by jemhtestfp@localhost.localdomain, with a hint of Encheferizer";
	}

	public void init(IJEMHFieldProcessorContext context)
	{
		fContext=context;
		fConfig=context.getConfig();
		fRelatedIssue=context.getRelatedIssue();
		fMessage=context.getMessage();
		fBody=context.getBody();
	}

	public String getVendor() {
		return "Mystery Vendor";
	}

	public String getVendorUrl() {
		return "http://wwww.vendor.com";
	}

	public IJEMHFieldProcessorContext getContext() {
		return fContext;
	}

	public boolean isHandlerOptOutOfJEMHPostConfig() {
		return true;
	}

	@Override
	public void handleDelete()
	{
		//nothing to clean up
	}

	@Override
	public void copyTo(int id)
	{
		//nothing to transfer
	}

	@Override
	public boolean isUsable() {
		return true;
	}

	@Override
	public String getStatus() {
		return "All ok";
	}
}
