package com.javahollic.jemh.example;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.ModifiedValue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder;
import com.javahollic.jira.emh.api.extension.cf.IJEMHCustomFieldAdapter;
import com.valiantys.jira.plugins.sql.customfield.SQLFeedContent;

/** Example custom field adapter, targetting the nFeed custom field.
 * 
 * This plugin allows JEMH to be decoupled from any 3rd party custom field specific processing.
 * 
 * @author andy
 *
 */
public class NFeedAdapter implements IJEMHCustomFieldAdapter {
	
	@Override
	public String getName() {
		return "nFeed Custom Field Adapter";
	}
	
	@Override
	public String getKey()
	{
		return "pluginpeople-cfadapter-nfeed";
	}

	@Override
	public String getDescription()
	{
		return "Provides specific functionality for nFeed Custom Fields";
	}

	@Override
	public Object getCustomFieldValue(Issue issue, CustomField nFeedCF, String customerValue)
	{
		SQLFeedContent content = (SQLFeedContent)issue.getCustomFieldValue(nFeedCF);
		if (content!=null)
		{
			content.clear();
			content.add(customerValue);
			nFeedCF.updateValue(null, issue, new ModifiedValue(null, customerValue ), new DefaultIssueChangeHolder());
		}
		else
		{
			// ?
		}
		return content;
	}

	@Override
	public boolean isSkipJemhUpdate()
	{
		return true;
	}

	@Override
	public String getVendor() {
		return "The Plugin People";
	}

	@Override
	public String getVendorUrl() {
		return "http://thepluginpeople.atlassian.net";
	}
}
