package com.javahollic.jemh.example;

import java.io.IOException;
import java.util.Properties;

import com.javahollic.jira.emh.api.IJEMHRuntimeConfig;
import com.javahollic.jira.emh.api.IJEMHUser;
import com.javahollic.jira.emh.api.extension.idprovider.IJEMHAlternateIdProvider;


public class ExampleIdProvider implements IJEMHAlternateIdProvider
{
	private Properties p=new Properties();
	
	public ExampleIdProvider()
	{
		try
		{
			p.load(getClass().getClassLoader().getResourceAsStream("example-users.txt"));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	@Override
	public IJEMHUser getUserByEmail(IJEMHRuntimeConfig config, String email)
	{
		IJEMHUser userData=new SimpleUser(config);
		
		String[] userAndDom=email.split("@");
		String dom=userAndDom[1];

		p.getProperty("userAndDom");
		
		return userData;
	}

	@Override
	public String getKey()
	{
		return "yourco-example-idprovider";
	}

	@Override
	public String getVendor()
	{
		return "Your co";
	}

	@Override
	public String getName()
	{
		return "Example 3rd party JEMH ID provider";
	}

	@Override
	public String getDescription()
	{
		return "Provide an example skeleton of an ID provider";
	}

	@Override
	public String getVendorUrl()
	{
		return "http://www.yourco.net";
	}

	@Override
	public String getStatus()
	{
		return "ok";
	}

	@Override
	public boolean isUsable()
	{
		return true;
	}
}
