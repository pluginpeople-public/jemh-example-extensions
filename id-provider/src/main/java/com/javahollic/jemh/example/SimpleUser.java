package com.javahollic.jemh.example;

import com.atlassian.jira.user.ApplicationUser;
import com.javahollic.jira.emh.api.IJEMHRuntimeConfig;
import com.javahollic.jira.emh.api.IJEMHUser;

public class SimpleUser implements IJEMHUser
{
	private String fUserId;
	private boolean fIsEnabled;
	private String fDisplayName;
	private ApplicationUser fJiraUser;
	private String[] fProxyAddresses;
	private JemhUserNameFormat fFullNameFormat=JemhUserNameFormat.firstNameSpaceLastName;
	private String fLastName;
	private String fFirstName;
	private String fEmail;
	private IJEMHRuntimeConfig fConfig;
	
	SimpleUser(IJEMHRuntimeConfig config)
	{
		fConfig=config;
	}

	@Override
	public String getUserId()
	{
		return fUserId;
	}

	@Override
	public void setUserId(String userId)
	{
		fUserId=userId;
	}

	@Override
	public String getEmail()
	{
		return fEmail;
	}

	@Override
	public void setEmail(String email)
	{
		this.fEmail = email;
	}

	@Override
	public String getFirstName()
	{
		return fFirstName;
	}

	@Override
	public void setFirstName(String firstName)
	{
		this.fFirstName = firstName;
	}

	@Override
	public String getLastName()
	{
		return fLastName;
	}

	@Override
	public void setLastName(String lastName)
	{
		this.fLastName = lastName;
	}

	@Override
	public String[] getProxyAddresses()
	{
		return fProxyAddresses;
	}

	@Override
	public void setProxyAddresses(String[] proxyAliases)
	{
		fProxyAddresses = proxyAliases;
	}

	@Override
	public void setDisplayName(String value) {
		fDisplayName=value;
	}
	@Override
	public String getDisplayName()
	{
		return fDisplayName;
	}
	
	@Override
	/** @return TRUE - enabled, FALSE - disabled, NULL, unknown/na */
	public boolean isUserEnabled() {
		return fIsEnabled;
	}

	@Override
	public void setUserEnabled(boolean isEnabled)
	{
		fIsEnabled=isEnabled;
	}

	@Override
	public ApplicationUser getJiraUser() {
		return fJiraUser;
	}

	@Override
	public String getFullName()
	{		
		return fDisplayName;
	}

	@Override
	public void setFullNameFormat(JemhUserNameFormat fullNameFormat)
	{
		fFullNameFormat=fullNameFormat;
	}

	@Override
	public JemhUserNameFormat getFullNameFormat()
	{
		return fFullNameFormat;
	}
}
