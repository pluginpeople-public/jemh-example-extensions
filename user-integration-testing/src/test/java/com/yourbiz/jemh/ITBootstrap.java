package com.yourbiz.jemh;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.StringAsset;
import org.jboss.shrinkwrap.api.importer.ZipImporter;
import org.jboss.shrinkwrap.api.spec.JavaArchive;

public class ITBootstrap 
{
	public static Archive<?> buildArchive(String jarName)
	{
		Archive<?> archive= ShrinkWrap.create(ZipImporter.class, jarName+ ".jar")
				.importFrom(new File("target/dependencies/greenmail-1.5.6.jar"))
				.as(JavaArchive.class)
		        .addPackages(true, AbstractJEMHIntegrationTest.class.getPackage())
			    .addPackages(true, "it.com.ppl.jemh.support")
			    .addPackages(true, "com.sun.mail.imap.protocol")
			    .addPackages(true, "com.sun.mail.iap");
	
		return archive;
	}
	
	/* http://arquillian.org/guides/shrinkwrap_introduction/ */
	public static void addAssets(Archive<?> archive, String[] assets) throws IOException
	{
		if(assets != null)
		{
			if (AbstractJEMHIntegrationTest.TEST_HOME.startsWith("/"))
			{
				System.err.println("Base path cannot start with / : "+AbstractJEMHIntegrationTest.TEST_HOME); 
				throw new RuntimeException("aborting process");
			}
				
			ClassLoader cl = AbstractJEMHIntegrationTest.class.getClassLoader();
			
			for (int i = 0; i < assets.length; i++)
			{
				InputStream is = cl.getResourceAsStream(assets[i]);
				if (is==null)
				{
					System.err.println("SysOut: AssetNotFound: "+assets[i]);				
					throw new IOException("Resource not found building archive: "+assets[i]);
				}
				String assetStr = IOUtils.toString(is);
				StringAsset timebombAsset=new StringAsset(assetStr);		
				archive.add(timebombAsset, assets[i]);
			} 
		}
		return;
	}
}
