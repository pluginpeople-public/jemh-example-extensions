package com.yourbiz.jemh.tests;

import java.io.IOException;
import java.io.InputStream;

import javax.mail.internet.MimeMessage;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.atlassian.jira.component.ComponentAccessor;
import com.javahollic.jira.emh.api.export.beans.ProfileBean;
import com.javahollic.jira.emh.api.integration.ITMailUtil;
import com.yourbiz.jemh.AbstractJEMHIntegrationTest;
import com.yourbiz.jemh.ITBootstrap;

@RunWith(Arquillian.class)
public class MessageParseTest extends AbstractJEMHIntegrationTest 
{
	private static final String MULTIPART_RELATED_TEXT_TEST_CASE = TEST_HOME + "/message-parse-test/multipart-related-text-testcase.txt";
	private static final String MULTIPART_RELATED_HTML_TEST_CASE = TEST_HOME + "/message-parse-test/multipart-related-html-testcase.txt";
	private static final String MULTIPART_RELATED_PROFILE = TEST_HOME + "/message-parse-test/multipart-related-profile.xml";
    
    private static final String USERNAME = "admin";
    
    // @Deployment methods run before the test class is instantiated and are used to create Archive for deployment to
    // an Arquillian container - in our case, we are deploying plugins to an Atlassian application.
    //

    //
    // This particular method will wrap this test class into a plugin.
    //
    @Deployment
    public static Archive<?> deployTests() {
    	Archive<?> archive = ITBootstrap.buildArchive(MessageParseTest.class.getName());
    	try
		{	
    		ITBootstrap.addAssets(archive, DEFAULT_RESOURCES);
    		ITBootstrap.addAssets(archive, new String[] {MULTIPART_RELATED_TEXT_TEST_CASE, MULTIPART_RELATED_HTML_TEST_CASE, MULTIPART_RELATED_PROFILE});
			System.out.println(archive.toString(true));
		}
		catch (IllegalArgumentException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
			System.exit(0);
		}
    	return archive;
    }

    //
    // Tests are executed on the SERVER - REMEMBER TO hit F5 on JEMH Project in Eclipse to reload classes to avoid CNFE
    //

    @Test
    @InSequence(10)
    public void testTextMessageContent() throws Exception
    {
    	InputStream is=getResourceAsStream(MULTIPART_RELATED_TEXT_TEST_CASE);
    	Assert.assertNotNull("Unable to load resource: "+MULTIPART_RELATED_TEXT_TEST_CASE, is);
    	MimeMessage message=reloadMessage(is);
    	Assert.assertNotNull("Unable to load message: ", message);
    	ITMailUtil mailUtils = fTestHarness.getMailUtil();

    	ComponentAccessor.getJiraAuthenticationContext().setLoggedInUser(ComponentAccessor.getUserManager().getUserByKey(USERNAME));
    	ProfileBean profileBean = installProfileXML(MULTIPART_RELATED_PROFILE, MessageParseTest.class+"-multipart-related", false);
    	Assert.assertNotNull("Profile was null: ", profileBean);
    	
    	String content=mailUtils.getContent(message, profileBean);
    	Assert.assertFalse("content was null", content.isEmpty());
    	Assert.assertTrue("content should contain 'Some example text'", content.indexOf("Some example text")!=-1);
    	return;
    }

    @Test
    @InSequence(20)
    public void testHtmlMessageContent() throws Exception
    {
    	InputStream is=getResourceAsStream(MULTIPART_RELATED_HTML_TEST_CASE);
    	Assert.assertNotNull("Unable to load resource: "+MULTIPART_RELATED_HTML_TEST_CASE, is);
    	MimeMessage message=reloadMessage(is);
    	Assert.assertNotNull("Unable to load message: ", message);
    	ITMailUtil mailUtils = fTestHarness.getMailUtil();
    	
    	ProfileBean profileBean = getProfile(MessageParseTest.class+"-multipart-related");
    	Assert.assertNotNull("Profile was null: ", profileBean);
    	
    	String content=mailUtils.getContent(message, profileBean);
    	Assert.assertFalse("content was null", content.isEmpty());
    	Assert.assertTrue("content should contain 'Some example text'", content.indexOf("Some example text")!=-1);
    	return;
    }

}