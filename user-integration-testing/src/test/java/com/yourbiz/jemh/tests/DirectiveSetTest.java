package com.yourbiz.jemh.tests;

import java.io.IOException;

import javax.mail.Message.RecipientType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.project.ComponentAssigneeTypes;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.UpdateProjectParameters;
import com.javahollic.jira.emh.api.export.beans.ProfileBean;
import com.javahollic.jira.emh.api.export.beans.TemplateSetBean;
import com.javahollic.jira.emh.api.integration.ITEventListenerManager;
import com.javahollic.jira.emh.api.integration.ITJiraMailServerManager;
import com.javahollic.jira.emh.api.integration.ITMailUtil;
import com.javahollic.jira.emh.api.integration.ITTemplateSetManager;
import com.javahollic.jira.emh.api.integration.ITWebUtil;
import com.javahollic.jira.emh.api.integration.beans.DirectiveSetBean;
import com.javahollic.jira.emh.api.integration.beans.EventListenerProjectMappingBean;
import com.yourbiz.jemh.AbstractJEMHIntegrationTest;
import com.yourbiz.jemh.ITBootstrap;

@RunWith(Arquillian.class)
public class DirectiveSetTest extends AbstractJEMHIntegrationTest
{
	private static final String PROFILE = TEST_HOME + "/directive-set-test/directive-enabled-profile.xml";
	private static final String TEST_CASE = TEST_HOME + "/directive-set-test/basic-testcase.txt";
	private static final String TEST_TEMPLATE_SET = TEST_HOME + "/directive-set-test/jemh-issueevent-template-2.xml";
	
	public static final String TEST_PROFILE_KEY = DirectiveSetTest.class.getName();
    private static final boolean IS_ENABLED = true;
    private static final String IN_REVIEW_DIRECTIVES = "@labels=approved\r\n"+
    												   "@priority=highest\r\n"+
    												   "@workflow=Start Progress\r\n";

    private static final String DONE_DIRECTIVES = "@labels=rejected\r\n"+
			   "@priority=lowest\r\n"+
			   "@workflow=Resolve Issue\r\n";

    private static final String CLICKBACK_URL = "/rest/jemh/latest/public/directiveset/invoke";
    
    private static final String USERNAME = "admin";
    
    // @Deployment methods run before the test class is instantiated and are used to create Archive for deployment to
    // an Arquillian container - in our case, we are deploying plugins to an Atlassian application.
    //

    //
    // This particular method will wrap this test class into a plugin.
    //
    @Deployment
    public static Archive<?> deployTests() {
    	Archive<?> archive = ITBootstrap.buildArchive(DirectiveSetTest.class.getName());
    	try
		{	
    		ITBootstrap.addAssets(archive, DEFAULT_RESOURCES);
    		ITBootstrap.addAssets(archive, new String[] {PROFILE, TEST_CASE, TEST_TEMPLATE_SET});
			System.out.println(archive.toString(true));
		}
		catch (IllegalArgumentException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
			System.exit(0);
		}
    	return archive;
    }

    //
    // Tests are executed on the SERVER - REMEMBER TO hit F5 on JEMH Project in Eclipse to reload classes to avoid CNFE
    //

    @Test
    @InSequence(10)
    public void associateJiraMailServerWithLocalSmtpServerTest() throws Exception
    {
    	//super.setupJiraForAndyMailtrapSmtpDelivery();
    	super.setupJiraForGreenmailSmtpDelivery();
    }
    
    @Test
    @InSequence(20)
    public void roundTripTest() throws Exception
    {   
    	ITJiraMailServerManager mailManager = fTestHarness.getJiraMailServerManager();
    	
    	//clean any history
    	greenMail.purgeEmailFromAllMailboxes();
    	
    	//Generate an issue notification by running an issue creation test case
    	String profileKey = this.getClass().getName();
    	    	
    	//
    	// Install the profile
    	//
    	ComponentAccessor.getJiraAuthenticationContext().setLoggedInUser(ComponentAccessor.getUserManager().getUserByKey(USERNAME));
    	installProfileXML(PROFILE, profileKey, false);
    	ProfileBean profile = fTestHarness.getProfileManager().getProfileByKey(profileKey);
    	
    	//Get default project referring PROFILE
    	Project p = ComponentAccessor.getProjectManager().getProjectObjByKeyIgnoreCase("DST");
    	
    	//update project with project lead default assignee
    	UpdateProjectParameters upp=UpdateProjectParameters.forProject(p.getId());
    	upp.assigneeType(ComponentAssigneeTypes.PROJECT_LEAD);
    	upp.leadUserKey("admin");
    	p=ComponentAccessor.getProjectManager().updateProject(upp);
    	
    	Assert.assertNotNull("Project DST was unexpectedly null!", p);
    	
    	// Creation of an EventListener removes existing configuration and clears the given Project Notification Scheme
    	ITEventListenerManager evlm = fTestHarness.getEventListenerManager();
    	EventListenerProjectMappingBean evlBean = evlm.createEventListenerEntry(p);
    	Assert.assertNotNull("EventListenerBean was unexpectedly null!", evlBean);
    	
    	//ensure eventlistener is set with the customfield_12345,customfield_12346 values for Sender Email Address and Email Participants
    	setupEventListenerCustomFields();

    	//clear historic DiretiveSets and Links
    	evlm.removeAllDirectiveSets();
    	evlm.removeAllDirectiveSetLinks();
    	    	
    	// Install the two DirectiveSets    	
    	String profileCatchemail=profile.getConfiguration().getCatchEmailAddress();
    	DirectiveSetBean approveDS=evlm.createDirectiveSetEntry(IS_ENABLED, "Approve DS", "call this to advance from [Open] to [In Progress]", profileCatchemail, IN_REVIEW_DIRECTIVES);
    	DirectiveSetBean rejectDS=evlm.createDirectiveSetEntry(IS_ENABLED, "Reject DS", "call this to advance from [Open] to [Closed]", profileCatchemail, DONE_DIRECTIVES);

    	DirectiveSetBean[] allBeans = evlm.getAllDirectiveSets();
    	Assert.assertTrue("Expected 2 DirectiveSets to be created, found "+allBeans.length, allBeans.length==2);

    	// Update TemplateSet with DirectiveSet and Profile ID's
    	//clear old templatesets
    	ITTemplateSetManager tsm = fTestHarness.getTemplateSetManager();
    	tsm.deleteAllTemplateSets();
    	TemplateSetBean templateSetBean = installTemplateSetXML(TEST_TEMPLATE_SET);
    	String workingHtml = templateSetBean.getHtml();
    	workingHtml=workingHtml.replaceAll("CLICKBACK_PROFILE_ID", ""+profile.getProfileId());
    	workingHtml=workingHtml.replaceAll("APPROVE_DIRECTIVE_SET_ID", ""+approveDS.getId());
    	workingHtml=workingHtml.replaceAll("REJECT_DIRECTIVE_SET_ID", ""+rejectDS.getId());
    	templateSetBean.setHtml(workingHtml);
    	tsm.updateTemplate(templateSetBean);
    	
    	// set event listener to use the newly created template set
    	evlBean.setJiraTemplate(templateSetBean.getIssueEvent(), templateSetBean.getId());
    	evlm.updateProjectMapping(evlBean);    	

    	//
    	// Run the test case
    	//
    	Issue createdIssue = installAndRunTestCase(TEST_CASE, profile).get(0).getIssue();
    	Assert.assertNotNull("No issue was created", createdIssue);
    	String issueDescription = createdIssue.getDescription();
    	Assert.assertNotNull("Description was null", issueDescription);
    	String issueStatus = createdIssue.getStatus().getName();
    	Assert.assertTrue("Created issue should be in Open status, was in : "+issueStatus, issueStatus.equals("To Do"));
    	
    	//
    	// Validate the outcome
    	//

    	// TODO Flush the issue created event through to the mailserver (greenmail) so it can be accessed;
    	mailManager.pushMailQueue();
    	greenMail.waitForIncomingEmail(5000, 1);

    	//assert that a directive link was created
    	/*int count =fTestHarness.getEventListenerManager().getDirectiveSetLinkCount();
    	Assert.assertTrue("Expected a directive set link to be generated, found "+count, count==2);*/
    	
    	MimeMessage[] messages = greenMail.getReceivedMessages();
    	ITMailUtil mailUtils = fTestHarness.getMailUtil();
    	for (int i = 0; i < messages.length; i++)
		{
    		MimeMessage reloaded=reloadMessage(messages[i]);
    		//logMessage(reloaded);
    		
    		InternetAddress addr=(InternetAddress)reloaded.getRecipients(RecipientType.TO)[0];
			if (addr.getAddress().equals("andy@localhost"))
			{
				//validate that the sender email doesn't contain the link
				String content = mailUtils.getContent(messages[i], profile);
				Assert.assertFalse("Content for reporter should not be empty", content.isEmpty());
				Assert.assertTrue("DirectiveSet link MUST NOT be present in mail to the originator", content.indexOf(CLICKBACK_URL)==-1);
			}
			else if (addr.getAddress().equals("admin@admin.com"))
			{
				String wikiContent = mailUtils.getContent(messages[i], profile);
				Assert.assertNotNull("Wiki content was null",wikiContent);
				
				String htmlContent = mailUtils.getExactContent(reloaded, true, profile);
				Assert.assertNotNull("RawHTML content was null",wikiContent);
				
				//validate the assignee DOES have the link.
				Assert.assertFalse("Content for assigneee with DirectiveSet link should not be empty", htmlContent.isEmpty());
				Assert.assertTrue("DirectiveSet link MUST be present in mail to the assignee", htmlContent.indexOf(CLICKBACK_URL)!=-1);	
				String href = mailUtils.getLinkHref(htmlContent, "a#jemh-ds-"+approveDS.getId());
	            Assert.assertNotNull("Link href was null", href);
				
	            //todo use HTTP client to click the link
	            ITWebUtil webUtil = fTestHarness.getWebUtil();
	            String response=webUtil.getUrl(href);
	            
	            //throw away notifications
	            mailManager.clearJiraMailQueue();
	            
	            Assert.assertNotNull("Expected to get a response from invocation",response);
	            Assert.assertTrue(response.contains("JEMH DirectiveSet action completed"));

	            //todo assert expected issue workflow state change
	            Issue refreshedIssue = ComponentAccessor.getIssueManager().getIssueObject(createdIssue.getKey());
	            String statusName = refreshedIssue.getStatus().getName();
	            Assert.assertTrue("Expected issue status to now be In Progress, was: "+statusName, "In Progress".equals(statusName));	
			}
		}
    	
    	return;
    }

}