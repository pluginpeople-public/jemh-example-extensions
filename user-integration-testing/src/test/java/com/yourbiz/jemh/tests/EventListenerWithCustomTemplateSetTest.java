package com.yourbiz.jemh.tests;

import java.io.IOException;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.io.IOUtils;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.Archive;
import org.jfree.util.Log;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ofbiz.core.entity.GenericEntityException;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.permission.management.ManagedPermissionSchemeHelper;
import com.atlassian.jira.permission.management.beans.GrantToPermissionInputBean;
import com.atlassian.jira.permission.management.beans.PermissionsInputBean;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.icegreen.greenmail.store.FolderException;
import com.javahollic.jira.emh.api.export.beans.ProfileBean;
import com.javahollic.jira.emh.api.export.beans.TemplateSetBean;
import com.javahollic.jira.emh.api.integration.ITEventListenerManager;
import com.javahollic.jira.emh.api.integration.ITJiraMailServerManager;
import com.javahollic.jira.emh.api.integration.beans.EventListenerProjectMappingBean;
import com.yourbiz.jemh.AbstractJEMHIntegrationTest;
import com.yourbiz.jemh.ITBootstrap;

/**
 * This test validates round trip issue create and comment email notification for create, that the outbound notification is the custom template, and that no additional (dupe notifications) for create are sent  
 * 
 * @author andy
 */
@RunWith(Arquillian.class)
public class EventListenerWithCustomTemplateSetTest extends AbstractJEMHIntegrationTest
{
	private static final String PROFILE = TEST_HOME + "/event-listener-custom-template/basic-profile.xml";
	private static final String TEST_CASE = TEST_HOME + "/event-listener-custom-template/basic-testcase.txt";
	private static final String TEMPLATE_SET= TEST_HOME + "/event-listener-custom-template/jemh-issueevent-template-1.xml";
	private static final String INHIBIT_VM_SCRIPT= TEST_HOME + "/event-listener-custom-template/el-inhibit-script.vm";
	
	public static final String TEST_PROFILE_KEY = EventListenerWithCustomTemplateSetTest.class.getName();
   
	private static final String CUSTOM_FIELD_ID="CUSTOM_FIELD_ID";
	
	public static enum TestNotifyMeConditions { perUserPreference, always, forSelectedEvents };
    
    private static final String USERNAME = "admin";
	
    // @Deployment methods run before the test class is instantiated and are used to create Archive for deployment to
    // an Arquillian container - in our case, we are deploying plugins to an Atlassian application.
    //

    //
    // This particular method will wrap this test class into a plugin.
    //
    @Deployment
    public static Archive<?> deployTests() {
    	Archive<?> archive = ITBootstrap.buildArchive(EventListenerWithCustomTemplateSetTest.class.getName());
    	try
		{	
    		ITBootstrap.addAssets(archive, DEFAULT_RESOURCES);
    		ITBootstrap.addAssets(archive, new String[] {PROFILE, TEST_CASE, TEMPLATE_SET, INHIBIT_VM_SCRIPT});
			System.out.println(archive.toString(true));
		}
		catch (IllegalArgumentException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
			System.exit(0);
		}
    	return archive;
    }

    //
    // Tests are executed on the SERVER - REMEMBER TO hit F5 on JEMH Project in Eclipse to reload classes to avoid CNFE
    //

    @Test
    @InSequence(10)
    public void associateJiraMailServerWithLocalSmtpServerTest() throws Exception
    {
    	//setupJiraForAndyMailtrapSmtpDelivery();

    	setupJiraForGreenmailSmtpDelivery();
    }
    
    @Test
    @InSequence(20)
    public void setupForEventListenerTest() throws Exception
    {
    	Long customEventId = fTestHarness.getEventListenerManager().getCustomEvent();
    	Assert.assertNotNull(customEventId);    	
    	
    	/////////////////
    	// load a profile
    	/////////////////
    	ITJiraMailServerManager mailManager = fTestHarness.getJiraMailServerManager();
    	mailManager.clearJiraMailQueue();
    	
    	//clean any history
    	greenMail.purgeEmailFromAllMailboxes();
    	
    	//Generate an issue notification by running an issue creation test case
    	String profileKey = this.getClass().getName();
    	
    	//
    	// Install the profile
    	//
    	ComponentAccessor.getJiraAuthenticationContext().setLoggedInUser(ComponentAccessor.getUserManager().getUserByKey(USERNAME));
    	installProfileXML(PROFILE, profileKey, false);
    	ProfileBean profile = fTestHarness.getProfileManager().getProfileByKey(profileKey);
    	Assert.assertNotNull("Profile Bean was null", profile);
    	
    	//ensure eventlistener is set with the customfield_12345,customfield_12346 values for Sender Email Address and Email Participants
    	setupEventListenerCustomFields();
    	
    	//clear notification schemes on project, fix projecft permissions 
    	Project pDef = ComponentAccessor.getProjectManager().getProjectObjByKeyIgnoreCase("ELDEF");
    	Assert.assertNotNull("Project ELDEF was unexpectedly null!", pDef);
    	setupNotificationsForProject(pDef);
    	
    	Project pOne = ComponentAccessor.getProjectManager().getProjectObjByKeyIgnoreCase("ELONE");
    	Assert.assertNotNull("Project ELONE was unexpectedly null!", pOne);
    	setupNotificationsForProject(pOne);
    	
    	///////////////////////////
    	// update with custom event
    	///////////////////////////
    	
    	profile.getConfiguration().setCustomEventOnCreate(customEventId.intValue());
    	fTestHarness.getProfileManager().updateProfile(profile);
    	
    	//check
    	ProfileBean freshProfile = fTestHarness.getProfileManager().getProfileByKey(profileKey);
    	Assert.assertTrue("Expected custom event, expected "+customEventId.intValue()+", got: "+freshProfile.getConfiguration().getCustomEventOnCreate(), freshProfile.getConfiguration().getCustomEventOnCreate()==customEventId.intValue());

    	// clear all historic templates
    	fTestHarness.getTemplateSetManager().deleteAllTemplateSets();
    	
    	// upload custom template set
    	TemplateSetBean templateSetBean = installTemplateSetXML(TEMPLATE_SET, customEventId.intValue());
    	int tsId = templateSetBean.getId();
    	Assert.assertNotNull("TemplateSetBean was unexpectedly null!", templateSetBean);
    	
    	List<TemplateSetBean> all = fTestHarness.getTemplateSetManager().getIssueEventTemplateSets();
    	boolean matched=false;
    	for (Iterator<TemplateSetBean> iterator = all.iterator(); iterator.hasNext();)
		{
			TemplateSetBean tsb = iterator.next();
			if (tsb.getId()==tsId)
			{
				matched=true;
				break;
			}
		}
    	Assert.assertTrue("Created templateSet ID#"+tsId+" could not be found in ALL the created templates", matched);
    	
    	//
    	// Creation of an EventListener removes existing configuration and clears the given Project Notification Scheme
    	//
    	ITEventListenerManager evlm = fTestHarness.getEventListenerManager();
    	
    	String inhibitCustomFieldId=getInhibitCustomField();
    	Assert.assertNotNull("Inhibit custom field id was null", inhibitCustomFieldId);
    	
    	EventListenerProjectMappingBean defBean = evlm.createEventListenerEntry(pDef);
    	Assert.assertNotNull("EventListenerBean (default) was unexpectedly null!", defBean);
    	setupEventListenerBeanWithCustomTemplateSet(customEventId, templateSetBean, defBean);
    	setupEventListnerBeanWithInhibitScript(inhibitCustomFieldId, defBean);
    	evlm.updateProjectMapping(defBean);
    	
    	EventListenerProjectMappingBean oneBean = evlm.createEventListenerEntry(pOne);
    	Assert.assertNotNull("EventListenerBean (one) was unexpectedly null!", oneBean);
    	setupEventListenerBeanWithCustomTemplateSet(customEventId, templateSetBean, oneBean);
    	setupEventListnerBeanWithInhibitScript(inhibitCustomFieldId, oneBean);
    	evlm.updateProjectMapping(oneBean);
    	
    	//create or get the reporter and a CC user to test initial notification and watcher notifications
    	ApplicationUser asender = createUserWithEmail("el-sender@localhost");
    	Assert.assertNotNull("Sender was null", asender);
    	
    	// leave tests to other methods
    	return;
    }

    private String getInhibitCustomField()
	{
    	String cfId=null;
    	
    	Collection<CustomField> createdBy = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectsByName("Created by");
    	
    	if (!createdBy.isEmpty())
    	{
    		cfId=createdBy.iterator().next().getId();
    	}
    	
		return cfId;
	}

	/*
     * Method is going to inject a vm script looking for a custom field with a given value 'Email', used to inhibit event 1 when JEMH creates the issue
     */
	private void setupEventListnerBeanWithInhibitScript(String inhibitCustomFieldId, EventListenerProjectMappingBean aBean) throws Exception
	{
		InputStream inhibitScriptIs=null;
		Exception ex=null;
		try
		{
			inhibitScriptIs = getResourceAsStream(INHIBIT_VM_SCRIPT);
			String inhibitScript = IOUtils.toString(inhibitScriptIs);
			//inject required customfield id
			
			inhibitScript = inhibitScript.replace(CUSTOM_FIELD_ID.subSequence(0, CUSTOM_FIELD_ID.length()), inhibitCustomFieldId);
			aBean.setNonJiraEventSelectorScript(inhibitScript);
			aBean.setJiraEventSelectorScript(inhibitScript);
		}
		catch (IOException e)
		{
			ex=e;
		}
		finally
		{
			if (inhibitScriptIs!=null)
			{
				IOUtils.closeQuietly(inhibitScriptIs);
			}
			if (ex!=null)
			{
				throw ex;
			}
		}
	}

	private void setupEventListenerBeanWithCustomTemplateSet(Long customEventId, TemplateSetBean templateSetBean, EventListenerProjectMappingBean aBean)
	{
		// apply template set in event listener
		aBean.setNonJiraTemplate(customEventId.intValue(), templateSetBean.getId());
		aBean.setJiraTemplate(customEventId.intValue(), templateSetBean.getId());

    	//set JIRA users to be notified on create
		aBean.setNotifyMeCondition(TestNotifyMeConditions.forSelectedEvents.name());
		aBean.setNotifyMeEvents(""+customEventId.intValue());
	}

    private void setupNotificationsForProject(Project p) throws GenericEntityException
	{
    	//stop default JIRA notifications
    	ComponentAccessor.getNotificationSchemeManager().removeSchemesFromProject(p);
    	
    	//add Reporter with permission to MANAGE_WATCHER, EDIT_OWN_COMMENTS to ALL permission schemes

    	// see managedpermissionscheme : ./jira-project/jira-components/jira-plugins/jira-rest/jira-rest-plugin/src/main/java/com/atlassian/jira/rest/internal/v2/permissions/ManagedPermissionSchemeResource.java

    	// {"permissionKeys":["MANAGE_WATCHERS"],"grants":[{"securityType":"reporter"}]}
    	ManagedPermissionSchemeHelper helper = ComponentAccessor.getComponent(ManagedPermissionSchemeHelper.class);
    	PermissionsInputBean inputBean=new PermissionsInputBean();
    	
    	List<String> permissionKeysRequired=new ArrayList<>();
    	permissionKeysRequired.add("MANAGE_WATCHERS");
    	permissionKeysRequired.add("EDIT_OWN_COMMENTS");
    	inputBean.setPermissionKeys(permissionKeysRequired);
    	
    	List<GrantToPermissionInputBean> grants=new ArrayList<>();
    	grants.add(new GrantToPermissionInputBean("reporter", null));
    	inputBean.setGrants(grants);
    	
    	//default scheme is id 0
    	Log.debug("Added [MANAGE_WATCHERS, EDIT_OWN_COMMENTS] permissions for reporter");
    	helper.addManagedPermissionSchemeGrants(ComponentAccessor.getUserManager().getUserByName("admin"), 0L, inputBean);
    	
    	return;
	}
    
	/*
     * Email will be to a series of email users and JIRA users.  As there is a custom event, we need to validate that JEMH doesn't respond to ISSUE_CREATED, only the CUSTOM_EVENT. 
     */
    @Test
    @InSequence(30)
    public void sendCustomCreatedEvent() throws RemoteException, MessagingException
    {
    	//clear the mail queue
    	ITJiraMailServerManager mailManager = fTestHarness.getJiraMailServerManager();
    	mailManager.clearJiraMailQueue();

    	//get the profile
    	String profileKey = this.getClass().getName();
    	ProfileBean profile = fTestHarness.getProfileManager().getProfileByKey(profileKey);
    	Assert.assertNotNull("Profile Bean was null", profile);

    	//
    	// Run the test case
    	//
    	Issue createdIssue = installAndRunTestCase(TEST_CASE, profile).get(0).getIssue();
    	String issueDescription = createdIssue.getDescription();
    	Assert.assertNotNull("Description was null", issueDescription);

    	String issueKey = createdIssue.getKey();
    	Assert.assertNotNull("Issue Key was null", issueKey);
    	String createdByFieldId = getInhibitCustomField();
    	String createdByValue = (String)createdIssue.getCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObject(createdByFieldId));
    	Assert.assertNotNull("Created By value was null", createdByValue);
    	
    	//
    	// Validate the outcome
    	//
    	
    	//Flush the issuec created event through to the mailser (greenmail) so it can be accessed;
    	mailManager.pushMailQueue();
    	
	    //Wait for mail to arrive
    	greenMail.waitForIncomingEmail(5000, 1);
	    
	    MimeMessage[] emails = greenMail.getReceivedMessages();
	   
	    System.out.println("----");
	    for (int i = 0; i < emails.length; i++)
		{
	    	String to = ((InternetAddress)emails[i].getRecipients(RecipientType.TO)[0]).getAddress();
			System.out.println("Mail to:" +to+ " : "+emails[i].getSubject());
		}
	    System.out.println("----");	    
	    
	    ///JIRA sender and JIRA assignee
	    Assert.assertTrue("Expected 2 emails in GreenMail, got: " + emails.length, emails.length == 2);

	    String subject1=emails[0].getSubject();
	    Assert.assertTrue("Expected ONLY custom create notification with CUSTOM_CONTENT present, got ["+subject1+"]", subject1.indexOf("CUSTOM_NOTIFICATION")!=-1);
	    
	   
		try
	    {
		    for (int i = 0; i < emails.length; i++)
			{
				Assert.assertNotNull("JEMH mail header X_JEMH_NOTIFICATION not found", emails[i].getHeader("x-JEMH-notification"));
			}
		    
		    String emailOnlyRecipient = ((InternetAddress)emails[0].getAllRecipients()[0]).getAddress();
		    Assert.assertTrue("Expected recipient[0] to be (non-jira reporter)  el-sender@localhost , got "+emailOnlyRecipient, emailOnlyRecipient.equals("el-sender@localhost"));
		    
		    String jiraRecipient = ((InternetAddress)emails[1].getAllRecipients()[0]).getAddress();
		    Assert.assertTrue("Expected recipient[1] to be (jira assignee) admin@admin.com , got "+jiraRecipient, jiraRecipient.equals("admin@admin.com"));
		    
		    //Delete all emails in the Jira Mail Queue and from Greenmails inbox
		    mailManager.clearJiraMailQueue();
		    greenMail.purgeEmailFromAllMailboxes();
		   	MimeMessage[] emails2 = greenMail.getReceivedMessages();
		    Assert.assertTrue("Expected 0 emails in GreenMail after, got: " + emails2.length, emails2.length == 0);
	    }
		catch (FolderException fe)
		{
			throw new RemoteException("FolderClosed problem: "+fe.getLocalizedMessage());
		}
    }

}