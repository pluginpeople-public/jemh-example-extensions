package com.yourbiz.jemh.tests;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.List;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.atlassian.jira.component.ComponentAccessor;
import com.javahollic.jira.emh.api.export.beans.ProfileBean;
import com.javahollic.jira.emh.api.integration.beans.ProcessingResultBean;
import com.yourbiz.jemh.AbstractJEMHIntegrationTest;
import com.yourbiz.jemh.ITBootstrap;

/**
 * Example integration test for JEMH.
 * Test contains two test methods which install a profile and run a test case.
 * 
 * NOTE: by default, this test will run against a JIRA running on localhost:8080 user user/pass:  admin:admin, see src/test/resources/arquillian.xml to customize
 * 
 * @author Andy Brook
 *
 */
@RunWith(Arquillian.class)
public class ExampleTest extends AbstractJEMHIntegrationTest {
	
	private static final String PROFILE = TEST_HOME + "/example/basic-profile.xml";
	private static final String TEST_CASE = TEST_HOME + "/example/basic-testcase.txt";
	public static final String TEST_PROFILE_KEY = ExampleTest.class.getName();
    
    private static final String USERNAME = "admin";
	
    // @Deployment methods run before the test class is instantiated and are used to create Archive for deployment to
    // an Arquillian container - in our case, we are deploying plugins to an Atlassian application.
    //

    //
    // This particular method will wrap this test class into a plugin.
    //
    @Deployment
    public static Archive<?> deployTests() {
    	Archive<?> archive = ITBootstrap.buildArchive(ExampleTest.class.getName());
    	try
		{	
    		ITBootstrap.addAssets(archive, DEFAULT_RESOURCES);
			ITBootstrap.addAssets(archive, new String[] {PROFILE, TEST_CASE});
			System.out.println(archive.toString(true));
		}
		catch (IllegalArgumentException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
			System.exit(0);
		}
    	return archive;
    }

    //
    // Tests are executed on the SERVER - REMEMBER TO hit F5 on JEMH Project in Eclipse to reload classes to avoid CNFE
    //
 
    /**
     * This test installs a profile from a defined path.
     * A JEMH licence is automatically installed in the testSetup() method in the superclass.
     * 
     * @throws Exception if profile install fails
     */
    @Test
    @InSequence(10)
    public void installProfile() throws Exception
    {    	
    	ProfileBean testProfileBean = null;
    	
    	// Install a profile from a given path, with a reference to a static key.
    	// The key can be used in subsequent tests to retrieve the installed profile
    	ComponentAccessor.getJiraAuthenticationContext().setLoggedInUser(ComponentAccessor.getUserManager().getUserByKey(USERNAME));
		testProfileBean = installProfileXML(PROFILE, TEST_PROFILE_KEY, true);
		
    	Assert.assertNotNull("Profile Bean was null", testProfileBean);
    	
    	return;
    }
    
    /**
     * This test installs and runs a test case against the profile installed in the previous test.
     * 
     * @throws RemoteException if test case fails to install
     */
    @Test
    @InSequence(20)
    public void runTestCase() throws RemoteException
    {
    	// We must first retrieve the profile installed in the previous test
    	// The profile can be retrieved via the static key defined earlier
    	ProfileBean testProfileBean = getProfile(TEST_PROFILE_KEY);
    	
    	// Install and run a test case against the profile installed earlier
     	List<ProcessingResultBean> processingResults = installAndRunTestCase(TEST_CASE, testProfileBean);
     	
     	// Assert one processing result
     	Assert.assertTrue("Expected 1 processing result, got: " + processingResults.size(), processingResults.size() == 1);

     	// Get the first processing result bean
     	ProcessingResultBean testCaseResult = processingResults.get(0);
     	
     	// Assert that the test case resulted in an issue being created
     	Assert.assertNotNull("No issues were created by testcase", testCaseResult.getIssue());

     	// Assert that the test case resulted in an issue being created
     	Assert.assertNotNull("No auditEventBean was created by testcase", testCaseResult.getAuditEventBean());

     	// Clear the mail queue to stop delivery failure messages
     	fTestHarness.getJiraMailServerManager().clearJiraMailQueue();
    }
}
