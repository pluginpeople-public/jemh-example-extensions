package com.yourbiz.jemh;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.runner.RunWith;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserDetails;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.mail.MailException;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.icegreen.greenmail.junit.GreenMailRule;
import com.icegreen.greenmail.util.ServerSetupTest;
import com.javahollic.jira.emh.api.export.beans.ProfileBean;
import com.javahollic.jira.emh.api.export.beans.ProfileGroupBean;
import com.javahollic.jira.emh.api.export.beans.TemplateSetBean;
import com.javahollic.jira.emh.api.integration.IJEMHIntegrationTestHarness;
import com.javahollic.jira.emh.api.integration.ITJiraMailServerManager;
import com.javahollic.jira.emh.api.integration.ITTestCaseManager;
import com.javahollic.jira.emh.api.integration.beans.ProcessingResultBean;

@RunWith(Arquillian.class)
public abstract class AbstractJEMHIntegrationTest 
{
	public static final String TEST_HOME="integration";
	
	public static String[] DEFAULT_RESOURCES= new String[] {
			TEST_HOME + "/hello.txt"
	};
	
    @Rule
    public final GreenMailRule greenMail = new GreenMailRule(ServerSetupTest.SMTP);
    
    @ComponentImport
    @Inject
    public IJEMHIntegrationTestHarness fTestHarness;
	
    public AbstractJEMHIntegrationTest()
    {
    	return;
    }
    
    protected void setupJiraForGreenmailSmtpDelivery() throws MailException, RemoteException
    {
    	//Get the mail server manager component
    	ITJiraMailServerManager mailManager = fTestHarness.getJiraMailServerManager();

	    //Create the outbound config in JIRA
    	long mailServerId = mailManager.createMailServer("GreenMail", "Test: GreenmailExample", "jira@testinstance.net", "JIRA", "localhost", "username", "password", 3025, false);
    	Assert.assertTrue("mailServerId should be non zero", mailServerId >0);
    }

    /**
     * Create a mail server connection in JIRA with the provided configuration
     * @param name the name of the server
     * @param desc the description for the server
     * @param from the from address to be used for sending of mail from the server
     * @param prefix the prefix to be prepended to the subject of emails sent from the server
     * @param hostname the hostname of the mail server being connected to
     * @param username the username to use when connecting to the server
     * @param pwd the password to use when connecting to the server
     * @param port the port to to use when connecting to the server
     * @param useTLS indicates whether the mail server connection should use TLS
     * @throws RemoteException
     */
    protected void setupJiraForSmtpDelivery(String name, String desc, String from, String prefix, String hostname, String username, String pwd, int port, boolean useTLS) throws RemoteException
    {
    	//Get the mail server manager component
    	ITJiraMailServerManager mailManager = fTestHarness.getJiraMailServerManager();

	    //Create the outbound config in JIRA
    	long mailServerId = mailManager.createMailServer(name, desc, from, prefix, hostname, username, pwd, port, useTLS);
    	Assert.assertTrue("mailServerId should be non zero", mailServerId >0);
    }
    
	protected void clean() throws Exception
	{
    	fTestHarness.getEventListenerManager().setEventListenerEnabled(true);    	
    	fTestHarness.getAuditingManager().setAuditingEnabled(true);
		return;
	}
    
	protected void setupEventListenerCustomFields()
	{
    	//locate custom field Id's
    	CustomFieldManager cfm = ComponentAccessor.getCustomFieldManager();
    	Collection<CustomField> emailSenderAddressField = cfm.getCustomFieldObjectsByName("Email Sender Address");
    	Collection<CustomField> emailParticipantField = cfm.getCustomFieldObjectsByName("Email Participants");
    	Assert.assertTrue("There can be only one: Email Sender Address, found "+emailSenderAddressField.size(), emailSenderAddressField.size()==1);
    	Assert.assertTrue("There can be only one: Email Participants, found "+emailParticipantField.size(), emailParticipantField.size()==1);
    	
   		CustomField firstEmailSenderAddressField = emailSenderAddressField.iterator().next();
   		CustomField firstParticipantField = emailParticipantField.iterator().next();    	
    	String csv=firstEmailSenderAddressField.getId()+","+firstParticipantField.getId();
    	fTestHarness.getEventListenerManager().setEmailCustomFields(csv);

	}
	//-- Support methods for this class    
	
	//-- Subclass support methods
	/**
	 * Installs an XML profile from a given path and returns a ProfileBean if profile install succeeded
	 * @param path local path of the profile XML file
	 * @param profileKey unique key to be assigned to the installed profile for retrieval in subsequent tests
	 * @param addDefaultNotificationScheme add the default notification scheme to created projects
	 * @return the ProfileBean of the installed profile
	 * @throws Exception if profile install failed
	 */
	protected ProfileBean installProfileXML(String path, String profileKey, boolean addDefaultNotificationScheme) throws Exception
	{
		ProfileBean bean = null;
		InputStream is = null;
		try
		{	
			is = getResourceAsStream(path);
			bean=fTestHarness.getProfileManager().installProfile(is, profileKey, addDefaultNotificationScheme);	
		} 
		catch (RemoteException e)
		{
			throw(e);
		}
		catch (Exception e)
		{
			throw(e);
		}
		finally
		{
			IOUtils.closeQuietly(is);
		}
		return bean;
	}
	
    protected ApplicationUser createUserWithEmail(String emailAddress) throws Exception
	{
    	ApplicationUser user=null;
    	try
    	{
	    	
	    	UserManager um = ComponentAccessor.getUserManager();
	    	Set<ApplicationUser> all = um.getAllUsers();
	    	for (Iterator<ApplicationUser> iterator = all.iterator(); iterator.hasNext();)
			{
				ApplicationUser au = iterator.next();
				if (au.getEmailAddress().equalsIgnoreCase(emailAddress))
				{
					user=au;
					break;
				}
			}
	    	
	    	if (user==null)
	    	{
	    		JiraAuthenticationContext jac = ComponentAccessor.getJiraAuthenticationContext();
	    		ApplicationUser startingUser = jac.getLoggedInUser();
	    		try
	    		{
	    			jac.setLoggedInUser(um.getUserByName("admin"));
	    			UserDetails details =new UserDetails(emailAddress,emailAddress).withEmail(emailAddress);
	    			user = um.createUser(details);
	    			GroupManager gm = ComponentAccessor.getGroupManager();
	    			gm.addUserToGroup(user, gm.getGroup("jira-software-users"));
	    		}
	    		finally
	    		{
	    			jac.setLoggedInUser(startingUser);
	    		}
	    	}
    	}
    	catch (Exception e)
    	{
    		throw new Exception("User creation failed: "+e.getLocalizedMessage());
    	}
		return user;
	}
	
	protected TemplateSetBean installTemplateSetXML(String path) throws Exception
	{
		return installTemplateSetXML(path, -1);
	}
	protected TemplateSetBean installTemplateSetXML(String path, int eventId) throws Exception
	{
		TemplateSetBean bean = null;
		InputStream is = null;
		try
		{	
			is = getResourceAsStream(path);
			bean=fTestHarness.getTemplateSetManager().installTemplateSet(is, eventId);
		} 
		catch (RemoteException e)
		{
			throw(e);
		}
		catch (Exception e)
		{
			throw(e);
		}
		finally
		{
			IOUtils.closeQuietly(is);
		}
		return bean;
	}
	
	/**
	 * Installs an XML file containing a profile group and returns a ProfileGroupBean if the install succeeded
	 * @param path the path to the XML file containing the profile group
	 * @param profileGroupKey the unique key for the profile group to be referred to in subsequent tests
	 * @param addDefaultNotificationScheme add the default notification scheme to created projects
	 * @return the ProfileGroupBean of the installed profile group
	 * @throws RemoteException if the profile group install failed
	 */
	protected ProfileGroupBean installProfileGroupXML(String path, String profileGroupKey, boolean addDefaultNotificationScheme) throws RemoteException
	{
		ProfileGroupBean profileGroupBean = null;
		InputStream is = null;
		
		try
		{	
			is = getResourceAsStream(path);
			profileGroupBean=fTestHarness.getProfileManager().installProfileGroup(is, profileGroupKey, addDefaultNotificationScheme);	
		} 
		catch (RemoteException e)
		{
			throw(e);
		}
		catch (Exception e)
		{
			throw(new RemoteException(e.getLocalizedMessage()));
		}
		finally
		{
			IOUtils.closeQuietly(is);
		}
		
		return profileGroupBean;
	}
	
	protected ProfileBean installProfileInputStream(InputStream is, String profileKey, boolean addDefaultNotificationScheme) throws RemoteException
	{
		ProfileBean bean = null;
		bean = fTestHarness.getProfileManager().installProfile(is, profileKey, addDefaultNotificationScheme);
		return bean;
	}
	
    protected ProfileBean getProfile(String profileKey) throws RemoteException
    {
    	ProfileBean bean = fTestHarness.getProfileManager().getProfileByKey(profileKey);
    	return bean;
    }
    
    protected ProfileGroupBean getProfileGroup(String profileGroupKey)
    {
    	ProfileGroupBean profileGroupBean = fTestHarness.getProfileManager().getProfileGroupByKey(profileGroupKey);
    	return profileGroupBean;
    }
	
	protected InputStream getResourceAsStream(String resourcePath)
	{
		InputStream is = getClass().getClassLoader().getResourceAsStream(resourcePath);
		return is;
	}
	
	/**
	 * Method to install a test case from a path and then execute it against a provided profile bean
	 * 
	 * @param testCaseResourcePath the full path of the test case
	 * @param profileBean the profile bean for the test case to be executed against
	 * @return an array of IssueResultBeans for created issues
	 * @throws RemoteException if test case install failed
	 */
    protected List<ProcessingResultBean> installAndRunTestCase(String testCaseResourcePath, ProfileBean profileBean) throws RemoteException
    {
    	ITTestCaseManager testCaseManager = fTestHarness.getTestCaseManager();
    	List<ProcessingResultBean> resultBeans=null;
    	
    	Assert.assertNotNull("TestCaseManager is null", testCaseManager);
    	InputStream is=null;
    	try
    	{
			is = getClass().getClassLoader().getResourceAsStream(testCaseResourcePath);
			Assert.assertNotNull("TestCase file not found", is);

			int tcId = testCaseManager.installTestCase(is, profileBean.getProfileId());
			
			Assert.assertTrue("TestCase id is not valid", tcId>0);
			
			resultBeans = testCaseManager.runTestCase(tcId, profileBean.getProfileId());
			Assert.assertFalse("TestCase resulted in no issues created/updated", resultBeans.isEmpty());
		}
		catch (RemoteException e)
		{
			System.err.println("Failed to uploadTestCase: "+e.getLocalizedMessage());
			throw (e);
		}
    	catch (Exception e)
    	{
			System.err.println("UnhandledException to uploadTestCase: "+e.getLocalizedMessage());
			throw new RemoteException(e.getLocalizedMessage());
    	}
    	finally
    	{
    		IOUtils.closeQuietly(is);
    	}
    	
    	return resultBeans;
    }

    protected List<ProcessingResultBean> runMessage(InputStream is, ProfileBean profileBean) throws RemoteException
    {
    	ITTestCaseManager testCaseManager = fTestHarness.getTestCaseManager();
    	List<ProcessingResultBean> resultBeans=null;
    	try
    	{
			resultBeans = testCaseManager.runMessage(is, profileBean.getProfileId());
		}
		catch (RemoteException e)
		{
			System.err.println("Failed to uploadTestCase: "+e.getLocalizedMessage());
			throw (e);
		}
    	catch (Exception e)
    	{
			System.err.println("UnhandledException to uploadTestCase: "+e.getLocalizedMessage());
			throw new RemoteException(e.getLocalizedMessage());
    	}
    	finally
    	{
    		IOUtils.closeQuietly(is);
    	}
    	
    	return resultBeans;
    }
    
    protected void logMessage(MimeMessage message)
    {
    	try
		{
    		System.err.println("\n--- message follows ---\n");
    		ByteArrayOutputStream baos=new ByteArrayOutputStream();
    		message.writeTo(baos);
    		File f=new File("/tmp/out.txt");
    		if (f.exists())
    		{
    			f.delete();
    		}
    		f.createNewFile();
    		IOUtils.write(baos.toByteArray(), new FileOutputStream(f));
    		String mail=new String(baos.toByteArray(), "UTF-8");
    		System.err.println(mail);
    		System.err.println("\n--- message ends ---\n");
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (MessagingException e)
		{
			e.printStackTrace();
		}
    }
    
    protected MimeMessage reloadMessage(MimeMessage message) throws IOException, MessagingException
    {
		ByteArrayOutputStream baos=new ByteArrayOutputStream();
		message.writeTo(baos);		
    	return fTestHarness.getTestCaseManager().getMessage(new ByteArrayInputStream(baos.toByteArray()));
    }
    protected MimeMessage reloadMessage(InputStream is) throws IOException, MessagingException
    {
    	return fTestHarness.getTestCaseManager().getMessage(is);
    }
    
    //-- Setup and teardown methods inherited by all tests to clear the JIRA mail queue and setup the environment
    @Before
    public void setupTest() throws Exception
    {
    	clean();
    	fTestHarness.getJiraMailServerManager().pushMailQueue();
    	fTestHarness.getJiraMailServerManager().clearJiraMailQueue();
    }
    
    @After
    public void teardownTest()
    {
    	fTestHarness.getJiraMailServerManager().pushMailQueue();
    	fTestHarness.getJiraMailServerManager().clearJiraMailQueue();
    }
}
