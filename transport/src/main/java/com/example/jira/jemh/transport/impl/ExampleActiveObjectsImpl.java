package com.example.jira.jemh.transport.impl;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.example.jira.jemh.transport.api.ao.ExampleActiveObjects;
import com.example.jira.jemh.transport.api.ao.ExampleConfigEntity;
import javax.inject.Inject;
import javax.inject.Named;

@Named("exampleActiveObjects")
public class ExampleActiveObjectsImpl implements ExampleActiveObjects
{
	@ComponentImport
	private final ActiveObjects activeObjects;

	@Inject
	public ExampleActiveObjectsImpl(final ActiveObjects activeObjects)
	{
		this.activeObjects = activeObjects;
	}

	@Override
	public ExampleConfigEntity[] getAllConfigs()
	{
		return activeObjects.find(ExampleConfigEntity.class);
	}

	@Override
	public ExampleConfigEntity getConfiguration(int configId)
	{
		return activeObjects.get(ExampleConfigEntity.class, configId);
	}

	@Override
	public ExampleConfigEntity createConfiguration()
	{
		return activeObjects.create(ExampleConfigEntity.class);
	}

	@Override
	public void deleteConfiguration(int configId)
	{
		ExampleConfigEntity configuration = getConfiguration(configId);
		if (configuration != null)
		{
			activeObjects.delete(configuration);
		}
	}
}
