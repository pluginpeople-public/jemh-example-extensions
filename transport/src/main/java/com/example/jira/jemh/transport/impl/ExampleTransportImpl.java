package com.example.jira.jemh.transport.impl;

import com.javahollic.jira.emh.api.config.ConfigField;
import com.javahollic.jira.emh.api.extension.transport.*;
import com.example.jira.jemh.transport.api.ao.ExampleActiveObjects;
import com.example.jira.jemh.transport.api.ao.ExampleConfigEntity;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ExampleTransportImpl implements IJEMHTransportProvider
{
	private static final Logger LOG = Logger.getLogger(ExampleTransportImpl.class);
	private static final String TRANSPORT_KEY = "example-transport-key";
	private static final String VENDOR_NAME = "example-vendor-name";

	private final ExampleActiveObjects activeObjects;

	public ExampleTransportImpl(ExampleActiveObjects activeObjects)
	{
		this.activeObjects = activeObjects;
	}

	@Override
	public TransportConfigWrapper getConfig(int i)
	{
		return wrap(activeObjects.getConfiguration(i));
	}

	@Override
	public List<TransportConfigWrapper> getAllConfigs()
	{
		List<TransportConfigWrapper> ret = new ArrayList<>();
		for (ExampleConfigEntity config : activeObjects.getAllConfigs())
		{
			ret.add(wrap(config));
		}
		return ret;
	}

	@Override
	public void validateConfig(TransportConfigWrapper transportConfigWrapper, Map<String, String> map)
	{
		//here you would validate that fields have correct values
		for (ConfigField field : transportConfigWrapper.getFields())
		{
			if ("name".equals(field.getName()) && (field.getValue() == null || field.getValue().isEmpty()))
			{
				map.put(field.getName(), "The name field is required to have a value!");
			}
		}
	}

	@Override
	public TransportConfigWrapper createConfig()
	{
		return wrap(activeObjects.createConfiguration());
	}

	@Override
	public void updateConfig(TransportConfigWrapper transportConfigWrapper)
	{
		ExampleConfigEntity config = activeObjects.getConfiguration(transportConfigWrapper.getConfigId());
		if (config != null)
		{
			for (ConfigField field : transportConfigWrapper.getFields())
			{
				if ("name".equals(field.getName()))
				{
					config.setName(field.getValue());
				}
			}
			config.save();
		}
	}

	@Override
	public void deleteConfig(int i)
	{
		activeObjects.deleteConfiguration(i);
	}

	@Override
	public void sendAdHocMessage(int transportConfigId, String subject, String content, List<String> recipients,
			Map<String, List<String>> params) throws TransportException
	{
		DefaultTransportNotificationItem item = new DefaultTransportNotificationItem(
				subject, content, TransportContentType.text, TRANSPORT_KEY, transportConfigId, 0, 0,
				null, 0, null, false);
		send(item);
	}

	@Override
	public String getIssueEventTemplate(long l, TransportContentType transportContentType)
	{
		//handle getting template for event id and content type
		return "template content for event id " + l + " and type " + transportContentType.name();
	}

	@Override
	public String getIssueEventSubject(long l)
	{
		//subject will not apply for majoritry of implementations. See needsSubjectTemplate.
		return "subject would go here if needed";
	}

	@Override
	public boolean needsSubjectTemplate()
	{
		return false;
	}

	@Override
	public boolean needsTextTemplate()
	{
		return true;
	}

	@Override
	public boolean needsHtmlTemplate()
	{
		return false;
	}

	@Override
	public void sendTestMessage(int i, String s) throws TransportException
	{
		LOG.info(s);
	}

	@Override
	public String getKey()
	{
		return TRANSPORT_KEY;
	}

	@Override
	public String getVendor()
	{
		return VENDOR_NAME;
	}

	@Override
	public String getVendorUrl()
	{
		return "vendorurl";
	}

	@Override
	public String getName()
	{
		return "Example Transport";
	}

	@Override
	public String getDescription()
	{
		return "Demonstrates addition of a notification transport using the JEMH API";
	}

	@Override
	public boolean isUsable()
	{
		//test the notification channel is operational
		return true;
	}

	@Override
	public String getStatus()
	{
		return "status text";
	}

	@Override
	public TransportStatus getTransportStatus(int i)
	{
		TransportStatus status = new TransportStatus();
		status.setServiceState(isUsable() ? TransportStatus.SERVICE_STATE.UP : TransportStatus.SERVICE_STATE.DOWN);
		return status;
	}

	@Override
	public void send(ITransportNotificationItem iTransportNotificationItem) throws TransportException
	{
		try
		{
			LOG.info(iTransportNotificationItem.getContent());
		}
		catch (Exception e)
		{
			throw new TransportException("A problem occurred!", e);
		}
	}

	@Override
	public String getHelpURL()
	{
		return "helpurl";
	}

	private TransportConfigWrapper wrap(ExampleConfigEntity configEntity)
	{
		List<ConfigField> fields = new ArrayList<>();
		ConfigField nameField = new ConfigField("name", configEntity.getName(), "The name of this example configuration", null, null);
		fields.add(nameField);
		return new TransportConfigWrapper(configEntity.getID(), TransportContentType.text, fields);
	}
}