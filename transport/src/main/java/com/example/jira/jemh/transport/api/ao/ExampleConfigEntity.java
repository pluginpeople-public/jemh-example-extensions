package com.example.jira.jemh.transport.api.ao;

import net.java.ao.Entity;
import net.java.ao.schema.Table;

@Table("CONFIG")
public interface ExampleConfigEntity extends Entity
{
	String getName();

	void setName(String name);
}
