package com.example.jira.jemh.transport.api.ao;

public interface ExampleActiveObjects
{
	ExampleConfigEntity[] getAllConfigs();

	ExampleConfigEntity getConfiguration(int configId);

	ExampleConfigEntity createConfiguration();

	void deleteConfiguration(int configId);
}
